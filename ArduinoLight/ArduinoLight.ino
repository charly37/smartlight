#include "ArduinoJson-v5.8.4.h"

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

class color
{
  public:
    color();
    void setRandome();
    bool operator==(const color &other) const;
    bool operator!=(const color &other) const;
    int _red;
    int _blue;
    int _green;
};

color::color()
{
  this->_red = 0;
  this->_blue = 0;
  this->_green = 0;
}

void color::setRandome()
{
  this->_red = random(0, 255);
  this->_blue = random(0, 255);
  this->_green = random(0, 255);
}

bool color::operator==(const color &other) const
{
  return ((this->_red == other._red) && (this->_blue == other._blue) && (this->_green == other._green));
}

bool color::operator!=(const color &other) const
{
  return !(*this == other);
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

class light
{
  public:
    void goToColor();
    void setRandome();
    void updateStatus();
    void displayLed();
    light(int iRedPin, int iGreenPin, int iBluePin);
    void goToColor(color aDestination);
    void printDebug();
    color generateRandColor();
    void setupLight();

    // color object
    color _color;
    color _destination_color;
    bool _drifting;
    bool _waitingBeforeNextDrift;
    int _mosfet_pwm_red_pin;
    int _mosfet_pwm_blue_pin;
    int _mosfet_pwm_green_pin;
    //Time between each delta color change when the process is drifting from one color to another
    const int _driftSpeed = 75;
    //Time waited by the drift process before choosing a new color and start drifiting
    const long _timeBetweenDriftSequence = 60000;
    //time of the last color drift reach
    unsigned long _lastColorReach;
};

light::light(int iRedPin, int iGreenPin, int iBluePin)
{
  this->_mosfet_pwm_red_pin = iRedPin;
  this->_mosfet_pwm_blue_pin = iBluePin;
  this->_mosfet_pwm_green_pin = iGreenPin;
  _drifting = true;
  _waitingBeforeNextDrift = false;
  _lastColorReach = millis();
}

void light::setupLight()
{
  pinMode(this->_mosfet_pwm_red_pin, OUTPUT);
  pinMode(this->_mosfet_pwm_blue_pin, OUTPUT);
  pinMode(this->_mosfet_pwm_green_pin, OUTPUT);
  this->displayLed();
}

color light::generateRandColor()
{
  color aColor;
  aColor.setRandome();
  return aColor;
}

void light::printDebug()
{
  Serial.print("_color:_red:");
  Serial.print(_color._red);
  Serial.print(",_green:");
  Serial.print(_color._green);
  Serial.print(",_blue:");
  Serial.print(_color._blue);
  Serial.println("");
  Serial.print("_destination_color:_red:");
  Serial.print(_destination_color._red);
  Serial.print(",_green:");
  Serial.print(_destination_color._green);
  Serial.print(",_blue:");
  Serial.print(_destination_color._blue);
  Serial.println("");
}

/*ostream& operator<< (ostream &out, color &iColor)
  {
    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << "(" << iColor._red << ", " <<
        iColor._blue << ", " <<
        iColor._green << ")";
    return out;
  }*/

void light::displayLed()
{
  analogWrite(this->_mosfet_pwm_blue_pin, this->_color._blue); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(this->_mosfet_pwm_green_pin, this->_color._green); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(this->_mosfet_pwm_red_pin, this->_color._red); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
}

void light::goToColor(color aDestination)
{
}

void light::updateStatus()
{
  if (this->_color != _destination_color)
  {
    //Serial.println("Current Color : ");
    //this->printDebug();

    if (this->_color._red != _destination_color._red)
    {
      //Serial.println("Update RED");
      if (this->_color._red < _destination_color._red)
      {
        //Serial.println("Increase RED");
        this->_color._red++;
      }
      else
      {
        //Serial.println("Decrease RED");
        this->_color._red--;
      }
    }
    if (this->_color._blue != _destination_color._blue)
    {
      //Serial.println("Update BLUE");
      if (this->_color._blue < _destination_color._blue)
      {
        //Serial.println("Increase BLUE");
        this->_color._blue++;
      }
      else
      {
        //Serial.println("Decrease BLUE");
        this->_color._blue--;
      }
    }
    if (this->_color._green != _destination_color._green)
    {
      //Serial.println("Update GREEN");
      if (this->_color._green < _destination_color._green)
      {
        //Serial.println("Increase GREEN");
        this->_color._green++;
      }
      else
      {
        //Serial.println("Decrease GREEN");
        this->_color._green--;
      }
    }
    this->displayLed();
  }
  else if (_drifting)
  {
    //We just finished a drift. We save the time to be able to wait 5s before starting the new one
    if(!_waitingBeforeNextDrift)
    {
      _lastColorReach = millis();
      _waitingBeforeNextDrift = true;
    }
    Serial.println("Time to drift ?");
    unsigned long currentMillis = millis();
    if (currentMillis - _lastColorReach >= _timeBetweenDriftSequence) 
    {
      _lastColorReach = currentMillis;
      Serial.println("Start new drift");
      _waitingBeforeNextDrift = false;
      this->_destination_color.setRandome();
    }
  }
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//Create the light object with pin 6,9,10.
light _light(10, 6, 9);

void setup() {
  //USB Serial port.
  Serial.begin(115200);

  //BT module serial port.
  Serial1.begin(115200);

  //Initialize the light.
  _light.setupLight();
}

void loop() {
  // Wait a bit
  delay(_light._driftSpeed);

  if (Serial1.available())
  {
    // We received something on BT module

    // Memory pool for JSON object tree.
    StaticJsonBuffer<800> jsonBuffer;

    // Root of the object tree.
    // It's a reference to the JsonObject, the actual bytes are inside the
    // JsonBuffer with all the other nodes of the object tree.
    // Memory is freed when jsonBuffer goes out of scope.
    JsonObject& root = jsonBuffer.parse(Serial1);

    // Test if parsing succeeds.
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }

    // Decode the information
    const char* commandType = root["command"];
    Serial.print("Receveid command from BT module :");
    Serial.println(commandType);

    if (strcmp(commandType, "setRgb") == 0)
    {
      _light._color._red = root["red"];
      _light._color._blue = root["blue"];
      _light._color._green = root["green"];

      _light._destination_color._red = root["red"];
      _light._destination_color._blue = root["blue"];
      _light._destination_color._green = root["green"];

      _light._drifting = false;

      Serial.print("Going to set R:");
      Serial.print(_light._color._red);
      Serial.print("Going to set B:");
      Serial.print(_light._color._blue);
      Serial.print("Going to set G:");
      Serial.println(_light._color._green);

      _light.displayLed();
    }
    else if (strcmp(commandType, "getRgb") == 0)
    {
      Serial.println("Entering getRgb");
      //BT server request the actual value of light.
      //Sending the actual light value

      // Create the root of the object tree.
      //
      // It's a reference to the JsonObject, the actual bytes are inside the
      // JsonBuffer with all the other nodes of the object tree.
      // Memory is freed when jsonBuffer goes out of scope.
      JsonObject& root2 = jsonBuffer.createObject();
      // Add values in the object
      //
      // Most of the time, you can rely on the implicit casts.
      // In other case, you can do root.set<long>("time", 1351824120);
      root2["r"] = _light._color._red;
      root2["b"] = _light._color._blue;
      root2["g"] = _light._color._green;
      root2["d"] = _light._drifting;
      root2.printTo(Serial1);
      Serial1.println();
      //String aResponse = "{r:" + String(_light._color._red) + ",b:" + String(_light._color._blue) + ",g:" + String(_light._color._green) + "}";
      //Serial1.println(aResponse);
      //Serial.println(aResponse);
    }
    else if (strcmp(commandType, "randColor") == 0)
    {
      //BT server request the actual value of light.
      //Sending the actual light value
      color aNewRandColor = _light.generateRandColor();
      _light._color = aNewRandColor;
      _light._destination_color = aNewRandColor;
      _light.displayLed();
      _light._drifting = false;
    }
    else if (strcmp(commandType, "colorDrif") == 0)
    {
      //BT server request the actual value of light.
      //Sending the actual light value
      _light._drifting = !_light._drifting;
    }
    else
    {
      Serial.println("Unknow command received on BT");
    }
  }

  _light.updateStatus();
}

//From https://www.arduino.cc/en/Tutorial/ColorCrossfader
/* BELOW THIS LINE IS THE MATH -- YOU SHOULDN'T NEED TO CHANGE THIS FOR THE BASICS

  The program works like this:
  Imagine a crossfade that moves the red LED from 0-10,
    the green from 0-5, and the blue from 10 to 7, in
    ten steps.
    We'd want to count the 10 steps and increase or
    decrease color values in evenly stepped increments.
    Imagine a + indicates raising a value by 1, and a -
    equals lowering it. Our 10 step fade would look like:

    1 2 3 4 5 6 7 8 9 10
  R + + + + + + + + + +
  G   +   +   +   +   +
  B     -     -     -

  The red rises from 0 to 10 in ten steps, the green from
  0-5 in 5 steps, and the blue falls from 10 to 7 in three steps.

  In the real program, the color percentages are converted to
  0-255 values, and there are 1020 steps (255*4).

  To figure out how big a step there should be between one up- or
  down-tick of one of the LED values, we call calculateStep(),
  which calculates the absolute gap between the start and end values,
  and then divides that gap by 1020 to determine the size of the step
  between adjustments in the value.
*/

/*int calculateStep(int prevValue, int endValue) {
  int step = endValue - prevValue; // What's the overall gap?
  if (step) {                      // If its non-zero,
    step = 1020/step;              //   divide by 1020
  }
  return step;
  }*/

/* The next function is calculateVal. When the loop value, i,
   reaches the step size appropriate for one of the
   colors, it increases or decreases the value of that color by 1.
   (R, G, and B are each calculated separately.)
*/

/*int calculateVal(int step, int val, int i) {

  if ((step) && i % step == 0) { // If step is non-zero and its time to change a value,
    if (step > 0) {              //   increment the value if step is positive...
      val += 1;
    }
    else if (step < 0) {         //   ...or decrement it if step is negative
      val -= 1;
    }
  }
  // Defensive driving: make sure val stays in the range 0-255
  if (val > 255) {
    val = 255;
  }
  else if (val < 0) {
    val = 0;
  }
  return val;
  }*/

/* crossFade() converts the percentage colors to a
   0-255 range, then loops 1020 times, checking to see if
   the value needs to be updated each time, then writing
   the color values to the correct pins.
*/

/*void crossFade(int color[3]) {
  // Convert to 0-255
  int R = (color[0] * 255) / 100;
  int G = (color[1] * 255) / 100;
  int B = (color[2] * 255) / 100;

  int stepR = calculateStep(prevR, R);
  int stepG = calculateStep(prevG, G);
  int stepB = calculateStep(prevB, B);

  for (int i = 0; i <= 1020; i++) {
    redVal = calculateVal(stepR, redVal, i);
    grnVal = calculateVal(stepG, grnVal, i);
    bluVal = calculateVal(stepB, bluVal, i);

    analogWrite(redPin, redVal);   // Write current values to LED pins
    analogWrite(grnPin, grnVal);
    analogWrite(bluPin, bluVal);

    delay(wait); // Pause for 'wait' milliseconds before resuming the loop

    if (DEBUG) { // If we want serial output, print it at the
      if (i == 0 or i % loopCount == 0) { // beginning, and every loopCount times
        Serial.print("Loop/RGB: #");
        Serial.print(i);
        Serial.print(" | ");
        Serial.print(redVal);
        Serial.print(" / ");
        Serial.print(grnVal);
        Serial.print(" / ");
        Serial.println(bluVal);
      }
      DEBUG += 1;
    }
  }
  // Update current values for next loop
  prevR = redVal;
  prevG = grnVal;
  prevB = bluVal;
  delay(hold); // Pause for optional 'wait' milliseconds before resuming the loop
  }*/
